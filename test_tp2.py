from tp2 import*

def test_box_create():
    b=Box()

def test_box_add():
    b=Box()
    b.add("truc1")
    b.add("truc2")
    
def test_box_in():
    b=Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "truc3" not in b
    
def test_box_remove():
	b=Box()
	b.add("truc1")
	b.add("truc2")
	b.add("truc3")

def test_box_open_close():
	b=Box()
	assert (b.is_open()==True)
	b.open()
	assert (b.is_open()==True)
	b.close()
	assert (b.is_open()==False)
	
	
def test_box_action_look():
	b=Box()
	b.add("truc1")
	b.add("truc2")
	b.add("truc3")
	assert (b.action_look()=="La boite contient : truc1, truc2, truc3")
	b.close()
	assert (b.action_look()=="La boite est fermé")

	
def test_box_size():
	assert (Thing(2).getVolume()==2)
	
def test_Thing_avoir_size():
	b=Box()
	table=Thing(2)
	b.add(table)
	assert(table.getVolume()==2)
	
def test_Thing_capacity():
	t=Thing(2)
	t.set_capacity(4)
	
def test_Thing_has_room_for():
	t=Thing(2)
	b=Box(2)
	assert(b.has_room_for(t)==True)
	b.add(t)
	assert(b.has_room_for(t)==False)
	b.add(t)
	

		
	

	
	 
	
